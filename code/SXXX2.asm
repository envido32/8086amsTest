; ================================================
; Universidad Tecnologica Nacional
; Facultad Regional Avellaneda
; ================================================
; Catedra: Tecnicas Digitales 3
; Docente: Ing. Ricardo Calabria
; Ayudante: Ing. Ariel Martin
; Alumno: Manuel Cajide Maidana
; Contacto: manuel.cajide.maidana@ieee.org
; ================================================
; Inicializacion
; ================================================

public sumalib
extrn cont

codigo1 SEGMENT			; Asociacion de registros de segmento
	ASSUME CS:codigo1

; ================================================
; TP1 Sistema 80x86: Modo Real
; Ej. 6) 
; Desarrollar un programa que cuente la cantidad de 
; veces que se ingresan por teclado las letras 
; “a, b y c” alfabéticas pulsadas antes de presionar
; "ENTER". Deberá emitir el mensaje "la cantidad de 
; letras a, b ó c ingresadas son:" e imprimir la cantidad.
; [Se aceptará max. 9 teclas, se valorará que sea “n” 
; números ingresados]. 
; La rutina que cuenta los caracteres (máx. 9) es una 
; lejana (librería pública) que se llamará Sxxx2. 
; ================================================
; Arranca el programa - Libreria
; ================================================

sumalib	proc
	inc cont
	retf
sumalib	endp

; ================================================
; Termina el programa 
; ================================================

codigo1 ENDS
	end